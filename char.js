var character = function() {
	return {
		names: {
			str: "STRENGTH", 
			dex: "DEXTERITY", 
			con: "CONSTITUTION", 
			int: "INTELLIGENCE", 
			wis: "WISDOM", 
			cha: "CHARISMA"
		}, 
		scores: {
			str: 8, 
			dex: 8, 
			con: 8, 
			int: 8, 
			wis: 8, 
			cha: 8
		}, 
		o_remain: 50, 
		remain: 50, 
		getScore: function(a) { return this.scores[a]; }, 
		getMod: function(a) { return Math.floor((this.scores[a] - 10) / 2); }, 
		getCost: function(a) { var r = this.getMod(a); return (r < 0) ? 1 : r + 1; }, 
		increase: function(a) {
			if(this.getCost(a) <= this.remain && this.getScore(a) < 18) {
				this.scores[a]++;
				this.remain -= this.getCost(a);
			}
		}, 
		decrease: function(a) {
			if(this.scores[a] > 8) {
				this.remain += this.getCost(a);
				this.scores[a]--;
			}
		}, 
		reset: function() {
			for(i in this.scores) {
				char.scores[i] = 8;
			}
			this.remain = this.o_remain;
		}
	};
}